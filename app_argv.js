const yargs = require('yargs');
const geocode = require('./geocode/geocode.js');
const weatherData = require('./weatherData/weatherData.js');

const argv = yargs
    .options({
        a: {
            demand: true,
            alias: 'address',
            describe: 'Address for which you want to get the weather information',
            string: true
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

geocode.getLocationDetails(argv.address, (loc_error, loc_result) => {
    if(loc_error) {
        console.log(loc_error);
    }
    else {
        var latitude = loc_result.latitude;
        var longitude = loc_result.longitude;

        weatherData.getWeatherData(latitude, longitude, (weather_err, weather_result) => {
            if(weather_err) {
                console.log(weather_err);
            } else {
                console.log(`\nIts currently ${weather_result.temperature}, but it feels like ${weather_result.feelsLike}\n`);
            }
        });
    }
});
