const request = require('request');

// Forecast.io api key: 693b720bca5edfe4161a240d9b7713e9
getWeatherData = (latitude, longitude, callback) => {

    var api_key = '693b720bca5edfe4161a240d9b7713e9';
    var address_data = api_key + '/' + latitude + "," + longitude;

    request({
        url: `https://api.darksky.net/forecast/${address_data}`,
        json: true
    }, (error, response, body) => {
        if(!error) {
            if(body.code === '400' || response.statusCode === 400) {
                callback("The given location is not found!!");
            } else {
                callback(undefined, {
                    time: body.currently.time,
                    summary: body.currently.summary,
                    icon: body.currently.icon,
                    precipitationProbability: body.currently.precipProbability,
                    temperature: body.currently.temperature,
                    feelsLike: body.currently.apparentTemperature,
                    humidity: body.currently.humidity,
                    windSpeed: body.currently.windSpeed,
                    pressure: body.currently.pressure,
                });
            }
        } else {
            callback("There's an error connecting to the server!!");
        }
    });
}

module.exports.getWeatherData = getWeatherData;
