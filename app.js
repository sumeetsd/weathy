const readline = require('readline');
const geocode = require('./geocode/geocode.js');
const weatherData = require('./weatherData/weatherData.js');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('Tell us, where you are? ', (answer) => {
    geocode.getLocationDetails(answer, (loc_error, loc_result) => {
        if(loc_error) {
            console.log(loc_error);
        }
        else {
            var latitude = loc_result.latitude;
            var longitude = loc_result.longitude;

            weatherData.getWeatherData(latitude, longitude, (weather_err, weather_result) => {
                if(weather_err) {
                    console.log(weather_err);
                } else {
                    var temperature = (weather_result.feelsLike-32) / 1.8;
                    var feelsLike = (weather_result.feelsLike-32) / 1.8;
                    console.log(`\nIts currently ${weather_result.temperature}\xB0F or ${temperature.toPrecision(2)}\xB0C, but it feels like ${weather_result.feelsLike}\xB0F or ${feelsLike.toPrecision(2)}\xB0C...\n`);
                }
            });
        }
    });
    rl.close();
});
