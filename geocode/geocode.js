const request = require('request');

getLocationDetails = (address, callback) => {

    encodedAdd = encodeURIComponent(address);
    request({
        url: `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAdd}`,
        json: true
    }, (error, response, body) => {
        if(!error) {
            if(body.status === 'ZERO_RESULTS') {
                callback('The location you entered is not found!!');
            }
            else if(body.status === 'OK') {
                callback(undefined, {
                    address: body.results[0].formatted_address,
                    latitude: body.results[0].geometry.location.lat,
                    longitude: body.results[0].geometry.location.lng
                });
            }
        }
        else {
            callback("Error connecting to the server");
        }
    });
}

module.exports.getLocationDetails = getLocationDetails;
